<!doctype html>
<html class="no-js" lang="en">
<head>
  <meta charset="utf-8" />
  <!-- Set the viewport width to device width for mobile -->
  <meta name="viewport" content="width=device-width" />
  <title>Bench-Bar &amp; Annual Conference</title>
  <!-- Schema.org markup for Google+ -->
	<meta itemprop="name" content="Bench-Bar & Annual Conference">
	<meta itemprop="description" content="The Philadelphia Bar Association's premier event for networking and socializing with colleagues and members of the judiciary.">
	<meta itemprop="image" content="http://benchbar.philadelphiabar.org/images/BB2018fb.jpg">

	<!-- Twitter Card data -->
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@PhilaBar">
	<meta name="twitter:title" content="Bench-Bar & Annual Conference">
	<meta name="twitter:description" content="The Philadelphia Bar Association's premier event for networking and socializing with colleagues and members of the judiciary.">
	<meta name="twitter:image:src" content="http://benchbar.philadelphiabar.org/images/BB2018soc.jpg">

	<!-- Open Graph data -->
	<meta property="og:title" content="Bench-Bar & Annual Conference" />
	<meta property="og:type" content="article" />
	<meta property="og:url" content="http://benchbar.philadelphiabar.org/" />
	<meta property="og:image" content="http://benchbar.philadelphiabar.org/images/BB2018fb.jpg" />
	<meta property="og:description" content="The Philadelphia Bar Association's premier event for networking and socializing with colleagues and members of the judiciary." />
	<meta property="og:site_name" content="Bench-Bar & Annual Conference" />
  <!-- Included CSS Files (Uncompressed) -->
  <!--
  <link rel="stylesheet" href="stylesheets/foundation.css">
  -->
  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Oswald" rel="stylesheet">
  <link rel="author" href="humans.txt" />
  <!-- Included CSS Files (Compressed) -->
  <link rel="stylesheet" href="stylesheets/foundation.min.css?v15.0">
  <link rel="stylesheet" href="stylesheets/owl.carousel.css" >
  <link rel="stylesheet" href="stylesheets/owl.theme.css" >
  <link rel="stylesheet" href="build/app.css?v5">
 <!-- Font Awesome -->
  <link rel="stylesheet" href="stylesheets/font-awesome.css">
  <!--[if IE 7]>
  <link rel="stylesheet" href="stylesheets/font-awesome-ie7.min.css">
  <![endif]-->
</head>
<body>
<header>
		<h1><a href="index.php"><img src="images/bb2018logo.svg" alt="Bench-Bar and Annual Conference"></a></h1>
</header>
<article>
	<div class="contain-to-grid sticky">
	<nav class="top-bar" data-topbar role="navigation" data-options="sticky_on: large">
	  <ul class="title-area">
	    <li class="name">
	      <h1 class="menu-title"><a href="index.php"><!-- Bench-Bar &amp; Annual Conference --></a></h1>
	    </li>
	     <!-- Remove the class "menu-icon" to get rid of menu icon. Take out "Menu" to just have icon alone -->
	    <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
	  </ul>
	  <section class="top-bar-section">
	    <!-- Right Nav Section -->
	    <ul class="right">
        <li><a href="#sponsors">Sponsors</a></li>
				<li class="divider"></li>
				<li><a href="attendees.php">Attendees</a></li>
        <li class="divider"></li>
        <li><a href="schedule.php">Schedule &amp; Highlights</a></li>
				<?php
				$schedulepage = "/schedule.php";
				$currentpage = $_SERVER['REQUEST_URI'];
					if($schedulepage==$currentpage) {
						include_once 'filterbutton.php';
					}
				?>
				<!-- <li class="has-form"><a href="https://www.philadelphiabar.org/page/EventDetails&eventID=BB2018" class="button disabled"><i class="icon-ok" aria-hidden="true"></i> Register</a></li> -->
	    </ul>
	  </section>
	</nav>
	</div>
</article>
<main>
<article>
  <section id="headerslider">
<div class="row">
	<div class="large-4 medium-4 small-6 info columns">
    <h2 class="subheader">Where</h2>
		<h3>Borgata, Atlantic City</h3>
	</div>
	<div class="large-4 medium-4 small-6 info columns">
		<h2 class="subheader">When</h2>
		<h3>October 12-13, 2018</h3>
	</div>
	<div class="large-4 medium-4 info columns">
		<h2 class="subheader">What</h2>
		<p>
		The <a href="http://www.philadelphiabar.org" target="_blank">Philadelphia Bar Association's</a> premier event for networking and socializing with colleagues and members of the judiciary.
		</p>
	</div>
</div>
	</section>
