<?php
include_once 'header.php';
 ?>

<section id="twentytwelve" class="section">
  <div class="row">
   	<div class="large-12 columns schedule_with_bg" >
      <div class="row">
        <div class="large-9 large-centered columns">
          <h1>2018 Attendees <a href="#note"><sup>*</sup></a></h1>
        </div>
      </div>
    </div> 
  </div>
</section>
 <section class="section">
   <div class="row hide-for-print">
   	<div class="large-12 columns schedule_with_bg" >
      <div class="row">
        <div class="large-9 large-centered columns">
    <form>
    <div class="row">
      <div class="large-12 columns">
      <div class="row collapse prefix-radius">
        <div class="small-3 columns">
          <span class="prefix">Search</span>
        </div>
        <div class="small-9 columns">
          <input type="text" id="search_input" placeholder="Type to filter names">
        </div>
      </div>
    </div>
    </div>
    </form>

    <ul id="search_list" class="no-bullet attendeelist">
    <?php
    $openfile = fopen("attendees.csv", "r");
    while (($eachline = fgetcsv($openfile)) !== false) {
            echo "<li>";
            echo "<strong>" . "$eachline[0]" . "</strong>" . "&nbsp;";
            if (empty($eachline[1])) {
              echo "&nbsp;";
            }
            else {
              echo "<span class=\"label radius secondary overflowfix\">" . "$eachline[1]" . "</span>";
            }
            echo "</li>\n";
    }
    fclose($openfile);
    ?>
    </ul>
    <a name="note"></a><strong class="subheader">* Please Note: The attendee list is not updated in real time.</strong>
  </div> <!-- small-9 -->
      </div> <!-- row -->
    </div> <!-- large-12 columns -->
  </div> <!-- row -->
</section>

 <?php
 include_once 'footer.php';
  ?>
