<section id="sponsors">
  <div class="row">
	<div class="large-12 medium-12 columns sections_with_bg">
		<div class="row">
			<div class="large-12 medium-12 columns">
				<h3>Sponsors</h3><a name="sponsors"></a>
		<div class="row">
			<div class="sponsor_row signature_sponsors">
				<div class="sponsor"><a href="http://www.courts.phila.gov/"><img src="images/sponsors/fjd-seal.jpg" alt="First Judicial District of Pennsylvania"></a></div>
				<div class="sponsor"><a href="https://www.cornerstonediscovery.com/"><img src="images/sponsors/CDlogoHoriz.jpg" alt="Cornerstone Discovery"></a></div>
			</div> <!-- end signature_sponsors-->
      <div class="sponsor_row platinum_sponsors">
				<div class="sponsor"><a href="https://www.abcbail.com/"><img src="images/sponsors/abc-bail-bonds-logo.jpg" alt="ABC Bail Bonds"></a></div>
				<div class="sponsor"><a href="http://bpuinvestments.com/main/"><img src="images/sponsors/bpu_logo.jpg" alt="BPU Investment Management Inc."></a></div>
				<div class="sponsor"><a href="https://www.veritext.com/"><img src="images/sponsors/Veritext_Logo.jpg" alt="Veritext"></a></div>
      </div>  <!-- end platinum_sponsors-->
			<div class="sponsor_row gold_sponsors">
				<div class="sponsor"><a href="https://www.lawpay.com/"><img src="images/sponsors/LawPay_logo.jpg" alt="LawPay"></a></div>
				<div class="sponsor"><a href="https://www.lexisnexis.com/en-us/gateway.page"><img src="images/sponsors/LexisNexis_Logo.jpg" alt="LexisNexis"></a></div>
				<div class="sponsor"><a href="https://www.provident.bank/"><img src="images/sponsors/PRO_2C_tag.jpg" alt="Provident Bank"></a></div>
			</div><!-- end gold_sponsors-->
			<div class="sponsor_row silver_sponsors">
				<div class="sponsor"><a href="http://www.belliaprinting.com/"><img src="images/sponsors/BelliaPrintDesign.jpg" alt="Bellia Print and Design Forensic"></a></div>
				<div class="sponsor"><a href="http://fleisherforensics.com/"><img src="images/sponsors/FleisherLogo.jpg" alt="Fleisher Forensics"></a></div>
				<div class="sponsor"><a href="https://www.jamsadr.com/"><img src="images/sponsors/JAMS.jpg" alt="JAMS"></a></div>
				<div class="sponsor"><a href="http://www.robsonforensic.com/"><img src="images/sponsors/RobsonForensic_logo-black.jpg" alt="Robson Forensic"></a></div>
				<div class="sponsor"><a href="http://www.philadelphiabar.org/page/StateCivilLitigationSection">State Civil Litigation Section</a></div>
			</div><!-- end silver_sponsors-->
			<div class="sponsor_row bronze_sponsors">
				<div class="sponsor"><a href="https://courtcall.com/"><img src="images/sponsors/CRTCL_Logo_Horizontal.jpg" alt="Court Call"></a></div>
				<div class="sponsor"><a href="https://www.lawcash.net/"><img src="images/sponsors/LawCash.jpg" alt="LawCash"></a></div>
				<div class="sponsor"><a href="http://www.mybarinsurance.com/philadelphiabar/"><img src="images/sponsors/usi.jpg" alt="USI Affinity"></a></div>
				<div class="sponsor"><a href="http://www.philadelphiabar.org/page/WorkersCompensationSection">Workers' Compensation Section</a></div>
				<div class="sponsor"><a href="https://yocierge.com/"><img src="images/sponsors/YoCierge-Legal-Tech-Logo.jpg" alt="YoCierge"></a></div>
			</div><!-- end bronze_sponsors-->
    </div>  <!-- end row-->
			</div>
    </div> <!-- end row-->
    <div class="row">
			<div class="large-12 medium-12 columns">
      <hr>
				<ul class="button-group stack-for-small">
					<li><a href="SponsorGridv2.pdf" class="secondary button">Sponsorship Info</a></li>
					<li><a href="sponsor_form.pdf" class="secondary button"><i class="icon-download-alt" aria-hidden="true"></i> Sponsor Form</a></li>
				    <li><a href="mailto:tmccloskey@philabar.org?subject=Bench-Bar%202018%20Sponsorship" class="secondary button"><i class="icon-envelope" aria-hidden="true"></i> Contact Us</a></li>
				</ul>
			</div>
		</div>
		<div class="row">
			<div class="large-12 medium-12 columns">
				<p>For sponsorship information contact, the Philadelphia Bar Association's Director of Meetings and Special Events, <a href="mailto:tmccloskey@philabar.org?subject=Bench-Bar%202018%20Sponsorship">Tracey McCloskey</a>.</p>
			</div>
		</div> <!-- end row -->

	</div>
  </div>
</section>



</article>

</main>


<footer>
	<div class="row">
		<div class="large-6 medium-6 footersecs columns">
			<h5>&copy; <?php echo date('Y')?> | Philadelphia Bar Associaiton</h5>
			<p><a href="http://www.philadelphiabar.org"><img src="images/PhiladelphiaBarAssociationLogo.png" width="128" height="25" alt="PhiladelphiaBarAssociationLogo"></a><br> 1101 Market St., Philadelphia, PA 19107 | (215) 238-6300 | <a href="http://www.philadelphiabar.org">www.philadelphiabar.org</a> <br/>
								<a href="http://www.philadelphiabar.org/page/PrivacyPolicy">Privacy Policy</a> | <a href="http://www.philadelphiabar.org/page/Confidentiality">Confidentiality Notice</a> | <a href="http://www.philadelphiabar.org/page/Disclaimer">Disclaimer</a> | <a href="http://www.philadelphiabar.org/page/AboutStaff">Contact Us</a>
								</p>
							<ul class="button-group stack-for-small">
								<li><a href="mailto:tmccloskey@philabar.org?subject=Bench-Bar%202018%20Feedback" class="secondary small button"><i class="icon-envelope" aria-hidden="true"></i> Send Us Your Feedback</a></li>
								<li><a href="#" data-reveal-id="cancellationModul" class="secondary small button"><i class="icon-remove-sign" aria-hidden="true"></i> Cancellation Policy</a></li>
							</ul>
		</div>


		<div id="cancellationModul" class="reveal-modal"  data-reveal aria-labelledby="Cancellation Policy" aria-hidden="true" role="dialog">
		  <h2 id="modalTitle">Cancellation and Refund Policy.</h2>
      <p>
        All cancellation requests must be submitted in writing to Tracey McCloskey at <a href="mailto:tmccloskey@philabar.org">tmccloskey@philabar.org</a>. The amount refunded for cancellation is dependent upon the date the cancellation request is received. Cancellations received on or before Sept. 13; full refund less a $50 processing fee. Cancellations received Sept. 14 through Oct. 8; 50% of the total registration fee will be refunded. Cancellations received after Oct. 8 are 100% nonrefundable.
      </p>
			<a class="close-reveal-modal" aria-label="Close">&#215;</a>
		</div>






		<div class="large-6 medium-6 footersecs columns">
			<h5>Bench-Bar Committee</h5>
			<ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
			  <li>Mary F. Platt</li>
			  <li><span class="label radius">Chancellor</span></li>
			</ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
			  <li>Rochelle M. Fedullo</li>
			  <li><span class="label radius">Chancellor-Elect</span></li>
			</ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
			  <li>A. Michael Snyder</li>
			  <li><span class="label radius">Vice Chancellor</span></li>
			</ul>
			<ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
			  <li>Julia Swain</li>
			  <li><span class="label radius">Co-Chair</span></li>
			</ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
			  <li>Robert W. Zimmerman</li>
			  <li><span class="label radius">Co-Chair</span></li>
			</ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
        <li>Hon. Sheila A. Woods-Skipper</li>
        <li><span class="label radius">Judicial Liaison</span></li>
      </ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
        <li>Hon. Karen Y. Simmons</li>
        <li><span class="label radius">CJE Liaison</span></li>
      </ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
        <li>Regina M. Foley</li>
        <li><span class="label radius">Committee Member</span></li>
      </ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
        <li>Arly Smith-Pearson</li>
        <li><span class="label radius">Committee Member</span></li>
      </ul>
      <ul class="block-grid committee small-block-grid-2 medium-block-grid-2 large-block-grid-2">
        <li>Eric H. Weitz</li>
        <li><span class="label radius">Committee Member</span></li>
      </ul>
		</div>
	</div>
</footer>
<script src="javascripts/vendor/modernizr.js?v5.0"></script>
<script src="javascripts/vendor/jquery.js?v5.0"></script>

 <!-- Fast Filter for Attendees -->
 <script src="javascripts/vendor/jquery.fastLiveFilter.js"></script>

  <!-- Included JS Files (Compressed) -->
  <script src="javascripts/foundation.min.js?v5.0"></script>
<script>
  $(document).foundation();
</script>

<!-- Adding Twitter Embedded Tweet Support -->
	<script async src="//platform.twitter.com/widgets.js" charset="utf-8"></script>

<!-- adding orbit -->

<script src="javascripts/vendor/owl.carousel.min.js" charset="utf-8"></script>

  <!-- Anything Extra with JS done here -->
  <script src="javascripts/app.js?v6.0"></script>


	 <script>
		var _gaq=[['_setAccount','UA-3369188-3'],['_trackPageview']];
		(function(d,t){var g=d.createElement(t),s=d.getElementsByTagName(t)[0];
		g.src=('https:'==location.protocol?'//ssl':'//www')+'.google-analytics.com/ga.js';
		s.parentNode.insertBefore(g,s)}(document,'script'));
	</script>

</body>
</html>
