<?php
  include_once 'header.php';
?>


  <section id="twentytwelve" class="section">

  <div class="row">
	<div class="large-12 columns sections_with_bg">
      <h3>About the Conference</h3>
	            <p>The Philadelphia Bar Association will hold its Bench-Bar &amp; Annual Conference on Friday, Oct. 12 and Saturday, Oct. 13 at Borgata in Atlantic City.
		</p>
		<p>
				The Bench-Bar &amp; Annual Conference remains the premier event for networking and socializing with colleagues and members of the judiciary, including the opportunity to earn CLE &amp; CJE credits through unique course offerings.
			</p>
	</div>
   </div>
</section>
<section class="section">
  <div class="row">
	<div class="large-12 columns sections_with_bg">
      <h3>Looking Back at Last Year's Conference - 2017</h3>
		<div class="row">
			<div class="large-6 medium-6 columns">
			<h4 class="subheader"><i class="icon-twitter" aria-hidden="true"></i> Tweets About 2017</h4>
			<ul class="accordion" data-accordion>
        <li class="accordion-navigation">
          <a href="#panel1"><h5><i class="icon-twitter-sign" aria-hidden="true"></i> Sharon Lopez</h5></a>
          <div id="panel1" class="content">
            <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/PhilaBar?ref_src=twsrc%5Etfw">@PhilaBar</a> <a href="https://twitter.com/hashtag/BenchBar17?src=hash&amp;ref_src=twsrc%5Etfw">#BenchBar17</a> Panel on Judges Grit &amp; Gripes <a href="https://twitter.com/KHJ_Esq?ref_src=twsrc%5Etfw">@KHJ_Esq</a> moderated as Judge Gantman, Judge Anders, DA Hodge, Atty Bluestine spoke. <a href="https://t.co/sBsPdShn9f">pic.twitter.com/sBsPdShn9f</a></p>&mdash; Sharon Lopez (@Sharon_R_Lopez) <a href="https://twitter.com/Sharon_R_Lopez/status/919182961752313856?ref_src=twsrc%5Etfw">October 14, 2017</a></blockquote>
          </div>
        </li>
        <li class="accordion-navigation">
          <a href="#panel7a"><h5><i class="icon-twitter-sign" aria-hidden="true"></i> Tara Phoenix</h5></a>
          <div id="panel7a" class="content">
            <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">A great audience right after <a href="https://twitter.com/hashtag/BenchBar17?src=hash&amp;ref_src=twsrc%5Etfw">#BenchBar17</a> for <a href="https://twitter.com/PhilaBar?ref_src=twsrc%5Etfw">@PhilaBar</a> RealProperty CLE, Reclaiming Vacant Properties, examining <a href="https://twitter.com/hashtag/LandBanks?src=hash&amp;ref_src=twsrc%5Etfw">#LandBanks</a>. <a href="https://t.co/xU7HFCAWvI">pic.twitter.com/xU7HFCAWvI</a></p>&mdash; Tara Phoenix (@TPHOENIXCLE) <a href="https://twitter.com/TPHOENIXCLE/status/920337009134178304?ref_src=twsrc%5Etfw">October 17, 2017</a></blockquote>
          </div>
        </li>
        <li class="accordion-navigation">
          <a href="#panel2a"><h5><i class="icon-twitter-sign" aria-hidden="true"></i> Deborah R. Gross</h5></a>
          <div id="panel2a" class="content">
            <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/BenchBar17?src=hash&amp;ref_src=twsrc%5Etfw">#BenchBar17</a> amazing turnout of interested lawyers mesmerized by <a href="https://twitter.com/JFormanJr?ref_src=twsrc%5Etfw">@jformanjr</a> explaining how lawyers can help change happen <a href="https://t.co/1UjSIVNL99">pic.twitter.com/1UjSIVNL99</a></p>&mdash; Deborah R. Gross (@2017chancellor) <a href="https://twitter.com/2017chancellor/status/918963442609991680?ref_src=twsrc%5Etfw">October 13, 2017</a></blockquote>
          </div>
        </li>
        <li class="accordion-navigation">
          <a href="#panel4a"><h5><i class="icon-twitter-sign" aria-hidden="true"></i> Shabrei Parker</h5></a>
          <div id="panel4a" class="content">
            <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr">“Make sure you’re always helping people. If you’re always helping people, then good will always come back to you.”<a href="https://twitter.com/kevmincey?ref_src=twsrc%5Etfw">@kevmincey</a> <a href="https://twitter.com/hashtag/BenchBar17?src=hash&amp;ref_src=twsrc%5Etfw">#BenchBar17</a></p>&mdash; Shabrei Parker (@ShabreiP_Esq) <a href="https://twitter.com/ShabreiP_Esq/status/918887244534046720?ref_src=twsrc%5Etfw">October 13, 2017</a></blockquote>
          </div>
        </li>
        <li class="accordion-navigation">
          <a href="#panel6a"><h5><i class="icon-twitter-sign" aria-hidden="true"></i> Maureen Farrell</h5></a>
          <div id="panel6a" class="content">
            <blockquote class="twitter-tweet" data-lang="en"><p lang="en" dir="ltr"><a href="https://twitter.com/hashtag/probate?src=hash&amp;ref_src=twsrc%5Etfw">#probate</a> <a href="https://twitter.com/hashtag/section?src=hash&amp;ref_src=twsrc%5Etfw">#section</a> represents <a href="https://twitter.com/hashtag/BenchBar17?src=hash&amp;ref_src=twsrc%5Etfw">#BenchBar17</a> <a href="https://twitter.com/hashtag/borgata?src=hash&amp;ref_src=twsrc%5Etfw">#borgata</a> <a href="https://twitter.com/hashtag/good?src=hash&amp;ref_src=twsrc%5Etfw">#good</a> times <a href="https://t.co/MGCHAe3Vvt">pic.twitter.com/MGCHAe3Vvt</a></p>&mdash; Maureen Farrell (@MaureenFarrel18) <a href="https://twitter.com/MaureenFarrel18/status/919350264682950657?ref_src=twsrc%5Etfw">October 14, 2017</a></blockquote>
          </div>
        </li>
			</ul>
			</div>
			<div class="large-6 medium-6 columns">
			<h4 class="subheader"><i class="icon-camera-retro" aria-hidden="true"></i> Photos of 2017</h4>
			<ul class="small-block-grid-3 medium-block-grid-4 large-block-grid-5 gallery" data-clearing>
        <li><a href="images/bb2017/bb17_1.jpg" class="th"><img src="images/bb2017/bb17_1_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_2.jpg" class="th"><img src="images/bb2017/bb17_2_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_3.jpg" class="th"><img src="images/bb2017/bb17_3_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_4.jpg" class="th"><img src="images/bb2017/bb17_4_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_5.jpg" class="th"><img src="images/bb2017/bb17_5_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_6.jpg" class="th"><img src="images/bb2017/bb17_6_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_7.jpg" class="th"><img src="images/bb2017/bb17_7_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_8.jpg" class="th"><img src="images/bb2017/bb17_8_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_9.jpg" class="th"><img src="images/bb2017/bb17_9_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_10.jpg" class="th"><img src="images/bb2017/bb17_10_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_11.jpg" class="th"><img src="images/bb2017/bb17_11_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_12.jpg" class="th"><img src="images/bb2017/bb17_12_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_13.jpg" class="th"><img src="images/bb2017/bb17_13_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_14.jpg" class="th"><img src="images/bb2017/bb17_14_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_15.jpg" class="th"><img src="images/bb2017/bb17_15_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_16.jpg" class="th"><img src="images/bb2017/bb17_16_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_17.jpg" class="th"><img src="images/bb2017/bb17_17_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_18.jpg" class="th"><img src="images/bb2017/bb17_18_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_19.jpg" class="th"><img src="images/bb2017/bb17_19_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_20.jpg" class="th"><img src="images/bb2017/bb17_20_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_21.jpg" class="th"><img src="images/bb2017/bb17_21_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_22.jpg" class="th"><img src="images/bb2017/bb17_22_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_23.jpg" class="th"><img src="images/bb2017/bb17_23_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_24.jpg" class="th"><img src="images/bb2017/bb17_24_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_25.jpg" class="th"><img src="images/bb2017/bb17_25_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_26.jpg" class="th"><img src="images/bb2017/bb17_26_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_27.jpg" class="th"><img src="images/bb2017/bb17_27_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_28.jpg" class="th"><img src="images/bb2017/bb17_28_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_29.jpg" class="th"><img src="images/bb2017/bb17_29_tbm.jpg" alt="gallery image"></a></li>
        <li><a href="images/bb2017/bb17_30.jpg" class="th"><img src="images/bb2017/bb17_30_tbm.jpg" alt="gallery image"></a></li>
      </ul>
			</div>
		</div>
  	</div>
  </div>
</section>
<section id="venue" class="section">
  <div class="row">
  <div class="large-12 columns sections_without_bg">
      <h3>Borgata - Atlantic City</h3>
    <div class="row">
		<div class="large-6 medium-6 columns">
			<h4 class="subheader">Address</h4>
			<p>
			<strong>Borgata</strong><br/>
			<a href="http://goo.gl/rgLr11" target="blank"><i class="icon-map-marker" aria-hidden="true"></i> 1 Borgata Way<br/>
			Atlantic City, NJ 08401<br/>
			(directions to Borgata)</a>
			</p>
		</div>
	  <div class="large-6 medium-6 columns">
			<h4 class="subheader">Contact</h4>
			<a href="tel:(609)317-1000" class="secondary button"><i class="icon-phone" aria-hidden="true"></i> (609) 317-1000</a>
			<a href="http://www.theborgata.com/" class="secondary button"><i class="icon-external-link" aria-hidden="true"></i> Borgata Website</a>
	  </div>
	 </div> <!-- row end		 -->

    </div> <!-- 12 col end	 -->
 </div> <!-- row end	 -->
  <div class="owl-carousel">
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgata1.jpg" alt="slide"></div>
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgata2.jpg" alt="slide"></div>
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgata4.jpg" alt="slide"></div>
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgata5.jpg" alt="slide"></div>
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgata6.jpg" alt="slide"></div>
      <div class="item"><img class="lazyOwl" data-src="images/slider/borgataopen.jpg" alt="slide"></div>
  </div>


</section>
<?php
  include_once 'footer.php';
?>
