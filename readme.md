# Bench-Bar 2018 Website

## In Development Use Gulp
CD into directory and run `gulp` watch files that will build into build folder with autoprefixer

## Uses
 * Simple PHP setup to include header/footer/etc...
 * Foundation 5
 * Font Awesome 3.0.2
 * Core Owl Carousel CSS File v1.3.3
 * Gulp Autoprefixer

### Get Started TODOs
- [x] Updated logo
- [ ] Update Style for 2018
- [x] Add Header info about Conference Stuff
- [ ] Add Features Section to Hompage
- [x] Add 2017 Photos to Homepage
- [x] Add Committee Info to footer
- [x] Add 2018 Sponsor Info
- [x] Add Cancellation Policy
- [x] Remove 2017 Assets

### Future TODOs
