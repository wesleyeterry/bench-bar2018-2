$(document).ready(function() {
	$('#filterOptions dd a').click(function() {
		// fetch the class of the clicked item
		var ourClass = $(this).attr('class');

    // reset the active class on all the buttons
    $('#filterOptions dd').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');

		if(ourClass == 'all') {
			// show all our items
			$('#ourHolder').children('div.item').show("slow");
		}
		else {
			// hide all elements that don't share ourClass
			$('#ourHolder').children('div:not(.' + ourClass + ')').hide("slow");
			// show all elements that do share ourClass
			$('#ourHolder').children('div.' + ourClass).show("slow");
		}
		return false;
	});
  $('#filterOptions2 dd a').click(function() {
    // fetch the class of the clicked item
    var ourClass = $(this).attr('class');

    // reset the active class on all the buttons
    $('#filterOptions2 dd').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');

    if(ourClass == 'all') {
      // show all our items
      $('.CLEHolder').children('div.cleitem').show("slow");
    }
    else {
      // hide all elements that don't share ourClass
      $('.CLEHolder').children('div:not(.' + ourClass + ')').hide("slow");
      // show all elements that do share ourClass
      $('.CLEHolder').children('div.' + ourClass).show("slow");
    }
    return false;
  });
  $('.timefilterOptions dd a').click(function() {
    // fetch the class of the clicked item
    var ourClass = $(this).attr('class');

    // reset the active class on all the buttons
    $('.timefilterOptions dd').removeClass('active');
    // update the active state on our clicked button
    $(this).parent().addClass('active');

    if(ourClass == 'all') {
      // show all our items
      $('#ourHolder').children('div.item').show("slow");
    }
    else {
      // hide all elements that don't share ourClass
      $('#ourHolder').children('div:not(.' + ourClass + ')').hide("slow");
      // show all elements that do share ourClass
      $('#ourHolder').children('div.' + ourClass).show("slow");
    }
    return false;
  });

	$(".owl-carousel").owlCarousel({
	       autoPlay: 3000, //Set AutoPlay to 3 seconds
				 items: 4,
	       itemsDesktop: [1199,3],
	       itemsDesktopSmall: [979,2],
				 lazyLoad : true,
				 itemsMobile:	[479,1]
	});

	$(function() {
			$('#search_input').fastLiveFilter('#search_list');
		});

	console.log('%c Hey there code reviewer. If you notice a bug or two please email them to me at wterry@philadelphiabar.org ', 'background: #F8F8F8; color: #A00000; font-size:200%');

});
