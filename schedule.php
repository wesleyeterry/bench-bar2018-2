<?php
  include_once 'header.php';
?>
<section id="twentytwelve" class="section">
  <div class="row">
	<div class="large-12 columns sections_with_bg">
    <a href="#" data-reveal-id="filterModul" class="button secondary sticky"><i class="icon-filter" aria-hidden="true"></i> Schedule Filters</a>
      <h3>Schedule &amp; Highlights</h3>
	  <p class="sched_lead">
        This year’s conference again boasts <span class="sched_highlighted">23 sessions</span> affording lawyers and judges up to <span class="sched_highlighted">9 CLE | CJE credits</span>, as well as <span class="sched_highlighted">9 CJE credits</span> that clearly offer something for everyone.  
    </p>
    
    <h4 class="subheader">“Passport” Giveaways</h4>
    <div class="row">
      <div class="small-6 large-centered columns">
          <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-2">
            <li><img src="images/ipad.jpg" alt="iPad Image"></li>
            <li><img src="images/echo.jpg" alt="Echo Image"></li>
          </ul>
      </div>
    </div>
    <p class="copy">
      Throughout this year’s Conference take a moment to visit and thank our sponsors. In addition to showing them our immense gratitude, find out more about the services they provide and how you or your firm may be able to utilize such services. By engaging with our sponsors, you’ll be eligible to win one of two “passport” giveaways, an Amazon Echo or Apple iPad.
    </p>
    <!-- <ul class="small-block-grid-1 medium-block-grid-2 large-block-grid-3 highlights">
        <li><div>Winning the Criminal Battle without Losing the Civil War </div></li>
        <li><div>Tweets, Posts, Chats, Instagram ‐‐ Not Your Grandfather’s Evidence! </div></li>
        <li><div>Discussing Damages: How to Maximize Your Client’s Recovery at Trial</div></li>
        <li><div>Moving Forward from #MeToo: Sexual Harassment at the Courthouse, Networking Events &amp; Beyond</div></li>
        <li><div>Hot Topics from the Ethics Hotline</div></li>
        <li><div>PFA Practice and Procedure in the First Judicial District</div></li>
        <li><div>Disabilities, Discipline, Delinquency and Access to Justice</div></li>
        <li><div>First Judicial District “Family Feud” Breakfast Plenary</div></li>
        <li><div>Education Session by Thomas R. Kline, Founding Partner of Kline &amp; Specter, P.C.,</div></li>
    </ul> -->
	</div>
   </div>
</section>
<section class="section">
  <div class="row">
   <div class="large-12 columns schedule_with_bg" id="ourHolder">
     <div class="item friday fri9am fri10am1130am fri1130am1pm fri1pm2pm fri215pm315pm fri330pm5pm fri5pm6pm fri6pm630pm fri71030pm">
       <h2 class="day">Friday, Oct. 12</h2>
     </div> <!-- item friday -->
   <div class="item friday fri9am panel">
         <h4 class="schedule_title">Conference Check-in Opens</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">9 a.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
         <p class="planner">
      </p>
   </div> <!--item-->
   <div class="item friday fri9am panel">
         <h4 class="schedule_title">Meet our Sponsors and Enjoy a Continental Breakfast</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">9 - 10:30 a.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
         <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/JAMS.jpg" alt="JAMS"></li>
           <li>
           <p class="planner">
            (Sponsored by JAMS)
          </p>
          </li>
        </ul>
        <p class="copy">Meet our valued sponsors who have made this year’s Bench-Bar &amp; Annual Conference possible. Also, we invite you to visit each of our sponsors throughout the Conference to get your “passport” stamped to enter a raffle to win one of two passport giveaways, an Amazon Echo or Apple iPad!</p>
        </div>
   </div> <!--item-->
   <div class="item friday fri10am1130am panel CLEHolder">
         <h4 class="schedule_title">Four Concurrent CLE | CJE Programs</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">10 - 11:30 a.m.</span></li>
         </ul>
         
     <div class="cleitem CLE substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Substantive<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Salon A</span></li>
          <li><a href="coursematerials/0445LA_WinningCrimBattle_CivWar.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Winning the Criminal Battle Without Losing the Civil War: Recognizing the Relationship Between Criminal and Civil Cases and Avoiding Pitfalls to Obtain Success in Both</h4>
           <p class="planner">
        (Presented by the Criminal Justice Section)
        </p>
        <p class="copy">
         This CLE program examines how to identify potential civil cases that may derive from a criminal case. Panelists address how to properly preserve your criminal client's civil rights so that he/she may successfully litigate a civil case after receiving a favorable criminal case verdict.
        </p>
         <h5 class="subheader">Course Planner/Panelist</h5>
         <ul class="square">
          <li><strong>Troy H. Wilson</strong> - Troy H. Wilson, Esquire, LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Priscilla E. Jimenez</strong> - Kline &amp; Specter, P.C</li>
          <li><strong>Prof. Teressa “Teri” Ravenell</strong> - Professor of Law, Villanova University Charles Widger School of Law</li>
          <li><strong>Hon. Sheila A. Woods-Skipper</strong> - President Judge Court of Common Pleas, First Judicial District of Philadelphia</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
           <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Substantive<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 1</span></li>
          <li><img src="images/sponsors/Veritext_Logo.jpg" alt="Veritext"></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">New Deposition Technology</h4>
          <p class="planner">
          (Sponsored and presented by Veritext Legal Solutions)
          </p>
          <p class="copy">
          Lawyers have been taking depositions since the beginning of time. It is recognized as one of the most common tasks that litigators do.  The hand typed, paper deposition of old is a far cry from the instant real-time, hyperlinked, multimedia depositions of today. Even though many lawyers today leverage technology for more effective depositions, it’s often confusing to know which tool works best for which case. This program will take the mystery out of the latest advancements in deposition technology.  Ensuring anyone can benefit from the tools of today’s modern deposition technology.
          </p>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
         <li><strong>Gina Hardin</strong> - Vice President, Mid Atlantic Sales, Veritext Legal Solutions</li>
         <li><strong>Patrice O’Riordan</strong> - JD, Veritext Legal Solutions</li>
         </ul>
     </div> 
     </div> <!-- cle item -->

      <div class="cleitem ethics substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>0.5 Ethics<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 2</span></li>
          <li><a href="coursematerials/0445LB_10MosLater_PublicAccessPolicy.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">10 Months Later – Lessons Learned From the Pennsylvania Public Access Policy</h4>
        <p class="planner">
        (Presented by the Law Practice Management Committee)
        </p>
          <p class="copy">
          Learn how the new public access policy has changed the way lawyers practice, how courts and attorneys process their filings &amp; how Philadelphia’s court processes requests for information! This CLE program will focus on how lawyers have used, and perhaps abused, the policy, with real-life examples. The program will also update attendees about changes that resulted from the policy and address the practical effects of the new policy. The speakers will demonstrate best practices for complying with the new Rules, including how to use Adobe Acrobat software to streamline your filings.  Finally, the program will address the requirements and filings as they relate to the First Judicial District, procedure and avoiding sanctions.
          </p>
         <h5 class="subheader">Course Planner/Panelist</h5>
         <ul class="square">
          <li><strong>Daniel J. Siegel</strong> - Law Offices of Daniel J. Siegel, LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Pamela A. Myers</strong> - Office Administrator &amp; Paralegal,Law Offices of Daniel J. Siegel, LLC</li>
          <li><strong>Dominic J. Rossi</strong> - Deputy Court Administrator, Legal Services, First Judicial District of Pennsylvania</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
      <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Substantive<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 3</span></li>
          <li><a href="coursematerials/0445LC_ImmigrationLawin2018.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Immigration Law in 2018: Can They Really Still Come to America?</h4>
          <p class="planner">
          (Presented by the Immigration Law Committee)
          </p>
          <p class="copy">
          This fast-paced CLE program provides an overview of how the following immigration issues are impacting our community and how we now must effectively advocate for our clients:
          </p>
          <ul>
            <li class="copy">Travel ban—what it is now;</li> 
            <li class="copy">Asylum seekers—Central Americans and others—procedures for applying for asylum, the new immigration judge quotas, the meaning of tattoos, etc;</li>
            <li class="copy">Criminal Law—discussing new issues and the threat of ICE waiting for clients;</li>
            <li class="copy">Sanctuary cities and enforcement;</li>
            <li class="copy"> The corporate client—increased I-9s, targeting employers who hire undocumented individuals, targeting hospitals and universities through FDNS investigations; and</li>
            <li class="copy">DACA.</li>
          </ul>
         <h5 class="subheader">Course Planner/Panelist</h5>
         <ul class="square">
         <li><strong>Wendy Castor Hess</strong> - Landau, Hess, Simon &amp; Choi, Co-Chair, Immigration Law Committee</li>
         <li><strong>Min S. Suh</strong> - Buchanan Ingersoll &amp; Rooney PC, Co-Chair, Immigration Law Committee</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Caleb U. Arnold</strong> - Immigration Counsel, Philadelphia District Attorney’s Office</li>
          <li><strong>Golnaz Fakhimi</strong> - Immigrants' Rights Attorney, ACLU of Pennsylvania</li>
          <li><strong>Ayodele Gansallo</strong> - Senior Staff Attorney, HIAS Pennsylvania</li>
          <li><strong>Wayne Sachs</strong> - Sachs Law Group, LLC</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
   </div> <!--item-->
   <div class="item friday fri1130am1pm panel">
         <h4 class="schedule_title">Conference Welcome &amp; Networking Lunch</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">11:30 a.m. - 1 p.m.</span></li>
           <li><span class="label warning radius">Ballroom D</span></li>
           <li><span class="label alert radius">Quarterly Meeting &amp; Bylaws Voting</span></li>
         </ul>
         <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/CDlogoHoriz.jpg" alt="Cornerstone Discovery"></li>
           <li>         
           <p class="planner">
          (Sponsored by Cornerstone Discovery)
          </p>
          </li>
        </ul>
        <p class="copy">Connect and engage with colleagues, sponsors and judges. Build long-lasting relationships and partnerships as you increase your professional network. You never know who you might meet!</p>
        <div class="panel callout radius voting">
          <p>
            <strong>
              During the lunch a Quarterly Meeting will be held for members of the Philadelphia Bar Association to vote to adopt amendments to the bylaws of the Association approved for submission by the Association's Board of Governors at its August 30, 2018 meeting. 
            </strong>
          </p>
        </div>
        </div>
   </div> <!--item-->
   <div class="item friday fri1pm2pm panel">
         <h4 class="schedule_title">Four Concurrent CLE | CJE Programs</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">1 - 2 p.m.</span></li>
         </ul>
           <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Salon A</span></li>
          <li><img src="images/sponsors/CDlogoHoriz.jpg" alt="Cornerstone Discovery"></li>
          <li><a href="coursematerials/0445LE_TweetsPostsChatsInstagram.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Tweets, Posts, Chats, Instagram ‐‐ Not Your Grandfather's Evidence! <em>How to Get It and How to Present It</em></h4>
           <p class="planner">
          (Sponsored by Cornerstone Discovery and presented by the Criminal Justice Section)
          </p>
          <p class="copy">
          Hear from those with expertise in the law and forensic technology in the courtroom about how to find social media evidence, how to preserve it, how to get it admitted in court and how to present it persuasively! Issues of software and hardware use—when you can do it yourself and when you need professionals! Pitfalls and perspectives. We will look at everything from Facebook and webpages, to Instagram and meta-data.
          </p>
         <h5 class="subheader">Course Planner</h5>
         <ul class="square">
          <li><strong>Thomas J. Innes, III</strong> - Defender Association of Philadelphia</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Prof. Jules Epstein</strong> - Professor of Law and Director of Advocacy Programs, Temple University Beasley School of Law</li>
          <li><strong>Louis Cinquanto, EnCE</strong> - Managing Director and Senior Certified Digital Forensic Examiner, Cornerstone Discovery</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
      <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 1</span></li>
          <li><a href="coursematerials/0445LF_SafeHarborintheGDPR.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Finding Safe Harbor in the GDPR Storm: What U.S. Litigators Need to Know</h4>
           <p class="planner">
          (Presented by the Business Litigation Committee)
          </p>
          <p class="copy">
          Just as we were starting to get comfortable with the European approach to data privacy, the rules changed. The European Union implemented the General Data Protection Regulation (“GDPR”) in May 2018, and its impact on discovery in U.S. litigation will be significant. A panel of e-discovery and data privacy practitioners will fill you in on the best practices for complying with the new rules, with a focus on clients in the insurance, higher education, health and financial sectors.
          </p>
         <h5 class="subheader">Moderator</h5>
         <ul class="square">
          <li><strong>Elizabeth S. Fenton</strong> - Saul Ewing Arnstein &amp; Lehr LLP, Co-Chair, Business Litigation Committee</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Alexander R. Bilus</strong> - Saul Ewing Arnstein &amp; Lehr LLP</li>
          <li><strong>Michele D. Hangley</strong> - Shareholder, Hangley Aronchick Segal Pudlin &amp; Schiller</li>
          <li><strong>Philip N. Yannella</strong> - Ballard Spahr LLP</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
    <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 2</span></li>
          <li><a href="coursematerials/0445LG_PhilaRealEstateTaxes.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Philadelphia Real Estate Taxes</h4>
           <p class="planner">
          (Presented by the Young Lawyers Division)
          </p>
          <p class="copy">
          The City of Philadelphia conducted another county-wide reassessment for tax year 2019, and many property owners were unhappy with their new assessed values. This CLE program will examine the  laws that govern real estate tax assessments and assessment appeals for Philadelphia County, which are comprised of three different state statutes, local ordinances, and city agency regulations. The program will also provide an overview of the differences between the  Philadelphia statutes and those in the rest of the Commonwealth, including the recent history of real estate tax assessments in Philadelphia highlighting how assessment practices and procedures have drastically changed from 2012 through today.
          </p>
         <h5 class="subheader">Course Planners</h5>
         <ul class="square">
          <li><strong>Amanda J. Dougherty, (Moderator)</strong> - Klehr Harrison Harvey Branzburg LLP</li>
          <li><strong>Stephanie Stecklair</strong> - Klehr Harrison Harvey Branzburg LLP</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Idee C. Fox</strong> - Supervising Judge, Court of Common Pleas</li>
          <li><strong>Robert N.C. Nix, III</strong> - Secretary, Board of Revision of Taxes, City of Philadelphia</li>
          <li><strong>James Vandermark</strong> - White and Williams, LLP, Formerly City of Philadelphia Law Dept., Tax Unit, Real Estate Division</li> 
         </ul>
     </div> 
     </div> <!-- cle item -->
    <div class="cleitem ethics">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Ethics<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 3</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Ethics: Obligations of ‘Supervising’ and ‘Supervised’ Lawyers</h4>
           <p class="planner">
          (Presented by the Professional Responsibility Committee)
          </p>
          <p class="copy">
           A lawyer serving in a supervisory role has a responsibility to adequately train lawyers  under supervision. And, a supervised lawyer has an independent obligation to comply with the Rules of Professional Conduct, not merely follow the directions of a senior lawyer. In this program, experienced panelists will present dilemmas raised for both supervising and supervised lawyers in several short sketches, portraying  how such issues can arise in the practice of law. The panelists will then analyze the issues and  provide practical guidance for practitioners.
          </p>
         <h5 class="subheader">Course Planner</h5>
         <ul class="square">
          <li><strong>W. Bourne Ruthrauff</strong> - Bennett, Bricklin &amp; Saltzburg LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Denis P. Cohen</strong> - Court of Common Pleas, Trial Division – Civil, First Judicial District</li>
          <li><strong>Hon. Mark I. Bernstein (Ret.)</strong> - Mediation &amp; Arbitration, Adjunct Professor, Drexel University Thomas R. Kline School of Law</li>
          <li><strong>Diana K. Ashton</strong> - Polsinelli PC</li>
          <li><strong>Deborah A. Winokur</strong> - Polsinelli PC</li> 
         </ul>
     </div> 
     </div> <!-- cle item -->

   </div> <!--item-->
   <div class="item friday panel">
         <h4 class="schedule_title">Refreshment Break</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">2 - 2:15 p.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
        <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/FleisherLogo.jpg" alt="Fleisher Forensics"></li>
           <li>         
           <p class="planner">
          (Sponsored by Fleisher Forensics)
          </p>
          </li>
        </ul>
        </div>

   </div> <!--item-->

   <div class="item friday fri215pm315pm panel">
         <h4 class="schedule_title">Four Concurrent CLE | CJE Programs</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">2:15 - 3:15 p.m.</span></li>
         </ul>
        <div class="cleitem ethics">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Ethics<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Salon A</span></li>
          <li><img src="images/sponsors/bpu_logo.jpg" alt="BPU Investment Management Inc."></li>
          <li><a href="coursematerials/0445LI_LIFEHAPPENS_BPU.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Planning for the Seen, the Unseen and the Perpetually Deferred</h4>
          <p class="planner">
          (Sponsored and presented by BPU Investment Management, Inc.)
          </p>
          <p class="copy">
          Retirement is not the only reason you exit the practice of law. In this CLE program, panelists not only explore the necessary steps associated with retiring from the law, they will also examine various scenarios that force an exit from the law.  Panelists provide a framework for prudently exiting your practice while protecting and providing for you, your family, your partners and importantly, your clients.  Poor financial wellness can have a devastating impact on your practice.  In this program, panelists connect financial wellness with productivity and examine how to prepare you for exiting the practice of law, whether you choose to or you must.
          </p>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Paul J. Brahim, CFP&reg;, AIFA&reg;</strong> - Chief Executive Officer, BPU Investment Management, Inc.</li>
          <li><strong>Sayde Joy Ladov</strong> - Former Chancellor, Dolchin, Slotkin &amp; Todd, P.C.</li>
         </ul>
     </div> 
     </div> <!-- cle item -->  
        <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 1</span></li>
          <li><a href="coursematerials/0445LJ_WildWorldJurySelection.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Wild World of Jury Selection</h4>
          <p class="planner">
          (Presented by the State Civil Litigation Section)
          </p>
          <p class="copy">
            Join your colleagues on a journey into the “Wild World of Jury Selection” with Judges Marlene F. Lachman and Lisa M. Rau.  The judges and experienced trial attorneys will offer insight and guidance into proper and improper styles in dealing with potential jurors, including an overview of the new case <em>Trigg v. Children’s Hospital of Pittsburgh</em>. Panelists will also address current rules that are in place and areas where a rule is not necessarily on point.  
          </p>
         <h5 class="subheader">Course Planner/Moderator</h5>
         <ul class="square">
          <li><strong>Jordan Strokovsky</strong> - McEldrew Young</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
         <li><strong>Hon. Marlene F. Lachman</strong> - Court of Common Pleas, Trial Division - Civil, First Judicial District of Pennsylvania</li>
         <li><strong>Hon. Lisa M. Rau</strong> - Court of Common Pleas, Trial Division - Civil, First Judicial District of Pennsylvania</li>
         <li><strong>Lee D. Rosenfeld</strong> - Messa &amp; Associates, P.C.</li>
         <li><strong>James P. Tolerico</strong> - Robert J. Casey, Jr. &amp; Associates</li>
         </ul>
     </div> 
     </div> <!-- cle item -->    
    <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 2</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">PFA Practice and Procedure in the First Judicial District:  Contemporary Challenges Faced by Judges, Lawyers and Litigants</h4>
          <p class="planner">
          (Presented by the Family Law Section)
          </p>
          <p class="copy">
          Protection from abuse practice in Philadelphia County is unique in many ways, for not only are most of the litigants unrepresented, many of them need interpreters.  And with the use of social media evidence soaring in these cases, family lawyers need to know the proper way to introduce and authenticate digital evidence showing threats of violence, intimidation and stalking. This CLE is designed for both those in general practice, and those who concentrate in the larger field of domestic relations.  How does one subpoena a police officer to testify at trial, and what if he/she is not available? How does one properly introduce a police report?  When can a victim testify to alleged incidents of abuse not raised in the PFA filing?  How common is it for judges to order interim support or custody as part of the final order of protection? Can a lawyer successfully move to seal a PFA record? These and other of-the-many questions will be addressed by a uniquely qualified panel of the bench and bar.
          </p>
         <h5 class="subheader">Course Planner</h5>
         <ul class="square">
          <li><strong>Julia Swain</strong> - Fox Rothschild LLP, 2018 Bench Bar and Annual Conference Co-Chair</li>
         </ul>
         <h5 class="subheader">Course Planner/Moderator</h5>
         <ul class="square">
          <li><strong>Mark A. Momjian</strong> - Momjian Anderer, LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Christopher Mallios</strong> - Court of Common Pleas, Family Division, First Judicial District of Pennsylvania</li>
          <li><strong>James Rocco</strong> - Rocco Law Offices, LLC</li>
          <li><strong>Susan Pearlstein</strong> - Philadelphia Legal Assistance</li>
         </ul>
     </div> 
     </div> <!-- cle item -->
        <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 3</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">The Value of Cybersecurity and Protecting Personal Information in the Digital Age</h4>
          <p class="copy">
          Lawyers need to be well versed in the potential issues and threats to data privacy and security posed by smart phones, smart watches, voice assistants and other “helpful” everyday tools including devices we use as part of the Internet of Things (IoT).  The presentation will discuss the current cyber and privacy landscape from a legal, technological, business and insurance viewpoint.  We will discuss best practices to protect yourself when it comes to cybersecurity, emphasizing the risks and heightened levels of scrutiny associated with protecting personally identifiable information (PII).  This panel will break down various cyber threats and situations to explore the various implications to all areas of business and organizations.
          </p>
         <h5 class="subheader">Moderator</h5>
         <ul class="square">
          <li><strong>Rebecca L. Rakoski</strong> - Co-Founder &amp; Managing Partner, XPAN Law Group, LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Jordan L. Fischer</strong> - Co-Founder &amp; Managing Partner, XPAN Law Group, LLC</li>
          <li><strong>Genelle Franklin</strong> - Partner, Fridie Law Group, LLC</li>
          <li><strong>Conor Gilsenan</strong> - Editor-In-Chief, All Things Auth</li>
          <li><strong>Austin Morris Jr.</strong> - Morris Risk Management LLC</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
   </div> <!--item-->

   <div class="item friday panel">
         <h4 class="schedule_title">Refreshment Break</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">3:15 – 3:30 p.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
          <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/RobsonForensic_logo-black.jpg" alt="Robson Forensic"></li>
           <li>         
           <p class="planner">
          (Sponsored by Robson Forensic)
          </p>
          </li>
        </ul>
        </div>
   </div> <!--item-->

   <div class="item friday fri330pm5pm panel">
         <h4 class="schedule_title">Four Concurrent CLE | CJE Programs</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">3:30 - 5 p.m.</span></li>
         </ul>
        <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Substantive<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Salon A</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Discussing Damages: How to Maximize or Limit Recovery</h4>
          <p class="planner">
          (Presented by the Trial Lawyers Association)
          </p>
          <p class="copy">
          Understanding the basis, scope and limitations for recovering damages is an important first step. After all, winning the battle but losing the war may not be what your client had in mind. With the endgame in mind, how does counsel calculate, present and prove the damage amounts the client should seek at trial or in arbitration?  This CLE program will examine damages using the following tactics, providing detail for each tactic of how to maximize or limit recovery:
          </p>
          <ul>
            <li class="copy">Methods to employ during discovery/ pre-trial;</li>
            <li class="copy">Explaining non-physical damages to the jury (the before vs. the after);</li>
            <li class="copy">Amplifying and mitigating the effects of “piss-off” factors; and</li>
            <li class="copy">Methods to employ at trial.</li>
          </ul>
         <h5 class="subheader">Course Planner/Panelist</h5>
         <ul class="square">
          <li><strong>Joseph L. Messa, Jr.</strong> - Messa &amp; Associates, P.C.</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Frederica A. Massiah-Jackson</strong> - Court of Common Pleas, Trial Division - Civil, First Judicial District of Pennsylvania</li>
          <li><strong>Andrew R. Duffy</strong> - Saltz Mongeluzzi Barrett &amp; Bendesky PC</li>
          <li><strong>Heather Hansen</strong> - O'Brien &amp; Ryan, LLP</li>
          <li><strong>William J. “Bill” Ricci</strong> - Ricci Tyrrell Johnson &amp; Grey</li> 
         </ul>
     </div> 
     </div> <!-- cle item -->      
    <div class="cleitem ethics">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Ethics<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 1</span></li>
          <li><img src="images/sponsors/LawPay_logo.jpg" alt="LawPay"></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Hot Topics From the Ethics Hotline</h4>
          <p class="planner">
          (Sponsored by LawPay and presented by the Professional Guidance Committee)
          </p>
          <p class="copy">
          The basics of disciplinary proceedings and ethics resources.  Issues seen and heard. 
          </p>
         <h5 class="subheader">Course Planners/Panelists</h5>
         <ul class="square">
          <li><strong>Josh J.T. Byrne</strong> - Partner, Swartz Campbell, LLC, Co-Chair, Professional Guidance Committee</li>
          <li><strong>Beth L. Weisser</strong> - Partner, Fox Rothschild LLP, Co-Chair, Professional Guidance Committee</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Ann M. Butchart</strong> - Court of Common Pleas, Trial Division – Civil, First Judicial District of Pennsylvania</li>
          <li><strong>Donna M. Snyder</strong> - Zone I, Office of Disciplinary Counsel</li>
          <li><strong>Robert S. Tintner</strong> - Partner, Fox Rothschild LLP</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
    <div class="cleitem substantive ethics">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>0.5 Ethics<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 2</span></li>
          <li><a href="coursematerials/0445LO_EstatePlanningSoloSm.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Estate Planning for the Solo And Small Firm Practitioner: How to Plan During Your Lifetime and How to Administer a Practice Upon Death</h4>
          <p class="planner">
          (Presented by the Probate Section, Tax Section and Solo, Small Firm Management Committee)
          </p>
          <p class="copy">
          One of life’s greatest certainties is that we will not all live forever. Death may therefore create tremendous difficulties for the heirs of solo practitioners and practitioners in small firms who do not adequately plan for the transition or sale of their practices upon their respective deaths. Practitioners from all sections of the bar will benefit from learning how to incorporate a practitioner’s law practice into his or her estate plan, and how to administer an estate or a solo practitioner or practitioner in a small firm to assure that the practitioner’s clients are properly and ethically transitioned. 
          </p>
         <h5 class="subheader">Course Planners</h5>
         <ul class="square">
         <li><strong>Justin H Brown</strong> - Partner, Pepper Hamilton LLP</li>
         <li><strong>Maureen M. Farrell</strong> - Maureen M. Farrell, Esquire</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Robert H. Louis</strong> - Saul Ewing Arnstein &amp; Lehr LLP</li>
          <li><strong>James R. Malone, Jr.</strong> - Post &amp; Schell, P.C.</li>
          <li><strong>Ku Yoo</strong> - Chang &amp; Yoo, LLP</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
        <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1.5 Substantive<br/>CLE | CJE Credits</strong></li>
          <li><span class="label warning radius">Studio 3</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Disabilities, Discipline, Delinquency and Access to Justice</h4>
          <p class="planner">
          (Presented by the Legal Rights of Persons with Disabilities and Education Law Committees)
          </p>
          <p class="copy">
          Intersection between disability, education and criminal justice with a focus on disability awareness for lawyers and judges. In this program, panelists will address specific nuances of certain disabilities and their effect on legal services. In school settings, disabled children are often subjected to physical restraints, prolonged isolation, and humiliation. Disproportionate discipline extends to those same students during police encounters that may occur either in or outside of school a setting. The failure of those in authority to understand and modify their responses to children with disabilities can lead to escalation, misunderstanding, trauma, loss of educational opportunity, injustice, cruelty, and, as we have seen in the news, even death. Field experts provide insight into specific ways in which  disciplinary discrimination is experienced by members of those communities, highlighting areas where the legal community can improve or intervene and address the challenge of mental health leading to criminal charges. 
          </p>
         <h5 class="subheader">Course Planner/Moderator</h5>
         <ul class="square">
          <li><strong>Lee B. Awbrey</strong> - Montgomery County Office of the Public Defender, Chair, Legal Rights of Persons with Disabilities Committee</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Dr. Beverly Frantz</strong> - Project Director, Criminal Justice and Sexuality Initiatives, Institute on Disabilities, Temple University</li>
          <li><strong>Maura McInerney</strong> - Education Law Center, National Legal Center on Youth Justice and Education</li>
          <li><strong>Jessica Oppenheim</strong> - Director, New Jersey Arc’s Criminal Justice Advocacy Program</li>
        </ul>
     </div> 
     </div> <!-- cle item -->  
   </div> <!--item-->

   <div class="item friday fri5pm6pm panel">
         <h4 class="schedule_title">CLE | CJE Program</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">5 - 6 p.m.</span></li>
          <li><span class="label warning radius">Ballroom D</span></li>
         </ul>
        <div class="cleitem ethics"> 
         <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Ethics<br/>CLE | CJE Credits</strong></li>
          <li><a href="coursematerials/0445LQ_EthicsPlenary_DealNoDeal.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Deal or No Deal</h4>
          <p class="copy">
            After explaining the facts of a real case, an audience member will be presented with a deal. Should the attendee take the deal &hellip; or take his or her chance at trial? This entertaining and interactive program, presented in a game show format with video clips, will cover multiple ethical situations including inappropriate contact with jurors, fist-fighting judges, lying attorneys, inappropriate romantic interludes with clients, substance abuse and much more.
          </p>
         <h5 class="subheader">Featured Speaker</h5>
         <img src="images/joel-about.jpg" alt="Speaker Image" class="speaker-image">
         <ul class="square">
          <li><strong>Joel Oster</strong> - President and General Counsel, Comedian of Law, LLC, Shawnee, KS</li>
         </ul>
     </div> 
     </div> <!-- cle item -->      
   </div> <!--item-->
   <div class="item friday fri6pm630pm panel">
         <h4 class="schedule_title">Martini Madness Cocktail Hour</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">6 - 6:30 p.m.</span></li>
           <li><span class="label warning radius">Pre-Function</span></li>
         </ul>
         <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/PRO_2C_tag.jpg" alt="Provident Bank"></li>
           <li>         
           <p class="planner">
          (Sponsored by Provident Bank)
          </p>
          </li>
        </ul>
        </div>
   </div> <!--item-->
   <div class="item friday fri71030pm panel">
         <h4 class="schedule_title">Dinner Reception and Party</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">7 - 10 p.m.</span></li>
           <li><span class="label warning radius">Premier</span></li>
         </ul>
        <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/CDlogoHoriz.jpg" alt="Cornerstone Discovery"></li>
           <li>         
           <p class="planner">
          (Sponsored by Cornerstone Discovery)
          </p>
          </li>
        </ul>
        </div>
   </div> <!--item-->
   <!-- END FRIDAY -->

   <div class="item saturday sat8am sat8301030am sat945am1045am sat11am12pm">
     <h2 class="day">Saturday, Oct. 13</h2>
   </div> <!-- item saturday -->
   <div class="item saturday sat8am panel">
         <h4 class="schedule_title">Breakfast</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">8 - 9:30 a.m.</span></li>
           <li><span class="label warning radius">Salon D</span></li>
           <li><span class="label alert radius">First Passport Giveaway</span></li>
         </ul>
        <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><img src="images/sponsors/abc-bail-bonds-logo.jpg" alt="ABC Bail Bonds"></li>
           <li>         
           <p class="planner">
          (Sponsored by ABC Bail Bonds)
          </p>
          </li>
        </ul>
        </div>

   </div> <!--item-->
   <div class="item saturday sat8301030am panel">
         <h4 class="schedule_title">CLE Program</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">8:30 - 9:30 a.m.</span></li>
           <li><span class="label warning radius">Salon D</span></li>
         </ul>
     <div class="cleitem">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 CLE | CJE Credit</strong></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Family Feud Style Plenary</h4>
          <p class="planner">
          (Presented by the First Judicial District)
          </p>
          <p class="copy">
          The First Judicial District has planned another innovative and entertaining breakfast plenary in a “Family Feud” format that will examine a variety of contemporary topics affecting the legal community.  Our host will pose questions on challenging legal issues to teams that include judges and lawyers who rule and practice in all areas and in all levels of the legal and judicial system.
          </p>
          <h5 class="subheader">Course Planners</h5>
         <ul class="square">
         <li><strong>Hon. Sheila A. Woods-Skipper</strong> - President Judge, Court of Common Pleas, First Judicial District of Philadelphia</li>
         <li><strong>Hon. Karen Y. Simmons</strong> - Philadelphia Municipal Court, First Judicial District of Philadelphia, Chair, Pennsylvania Continuing Judicial Education Board</li>
         </ul>
          <h5 class="subheader">Host</h5>
         <ul class="square">
         <li><strong>James  A. Funt</strong> - Partner, Greenblatt, Pierce, Funt &amp; Flores, LLC</li>
         </ul>
          <h5 class="subheader">Team Panelists</h5>
         <ul class="square">
         <li><strong>Hon. Christine M. Hope</strong> - Philadelphia Municipal Court, First Judicial District of Pennsylvania</li>
         <li><strong>Hon. Christopher Mallios</strong> - Court of Common Pleas, Family Division, First Judicial District of Pennsylvania</li>
         <li><strong>Hon. Maria McLaughlin</strong> - Superior Court of Pennsylvania</li>
         <li><strong>Hon. Carolyn H. Nichols</strong> - Superior Court of Pennsylvania</li>
         <li><strong>Keir Bradford-Grey</strong> - Chief Defender, Defender Association of Philadelphia</li>
         <li><strong>Albert S. Dandridge, III</strong> - Former Chancellor, Partner, Schnader Harrison Segal &amp; Lewis LLP</li>
         <li><strong>Rochelle M. Fedullo</strong> - Chancellor-Elect, Partner, Wilson Elser Moskowitz Edelman &amp; Dicker LLP</li>
         <li><strong>Lawrence S. Krasner</strong> - District Attorney of Philadelphia</li>
         <li><strong>Marcel S. Pratt</strong> - City Solicitor, City of Philadelphia</li>
         <li><strong>Amber M. Racine</strong> - Raynes Lawn Hehmeyer</li>     
         </ul>
     </div> 
     </div> <!-- cle item --> 

   </div> <!--item-->
   <div class="item saturday panel">
         <h4 class="schedule_title">Break</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">9:30 - 9:45 a.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
         <div class="panel cle radius">
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li>
           <p class="planner">
          (Sponsored by the State Civil Litigation Section)
          </p>
          </li>
        </ul>
          </div>
   </div> <!--item-->
   <div class="item saturday sat945am1045am panel">
         <h4 class="schedule_title">4 Concurrent CLE | CJE Programs</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">9:45 - 10:45 a.m.</span></li>
         </ul>
      <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Salon A</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Moving Forward From #MeToo: Sexual Harassment at the Courthouse, Networking Events &amp; Beyond</h4>
          <p class="planner">
          (Presented by the Women's Rights Committee)
          </p>
          <p class="copy">
          Workplace sexual harassment occurs beyond the walls of the office. It happens in courthouses, at networking events, during work-related travel and in many other activities associated with the legal profession. In this course, the panel will discuss the laws that apply to sexual harassment in the workplace, the legal obligations law firms and other employers may have to prevent and address sexual harassment that occurs outside of the office, and what steps stakeholders in the profession can take to promote gender equity and prevent sexual harassment in all legal environments.
          </p>
         <h5 class="subheader">Course Planners/Panelists</h5>
         <ul class="square">
          <li><strong>Amal M. Bass</strong> - Women's Law Project, Co-Chair, Women's Rights Committee</li>
          <li><strong>Catherine T. Barbieri</strong> - Fox Rothschild LLP, Co-Chair, Women's Rights Committee</li>
         </ul>
         <h5 class="subheader"><strong data-tooltip aria-haspopup="true" class="has-tip" title="Additional faculty may be added">Panelists *</strong></h5>
         <ul class="square">
          <li><strong>Hon. Stella M. Tsai</strong> - Court of Common Pleas, Trial Division – Criminal, First Judicial District</li>
          <li><strong>Rachel M. Keene</strong> - Associate General Counsel, NAVIENT</strong></li>
          <li><strong>Carol Tracey</strong> - Executive Director, Women's Law Project</li>
          <li><strong>Jeff Campolongo</strong> - Law Office of Jeffrey Campolongo</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
         <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 1</span></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">The Separation of Powers: What Is the Future? --Teaching America’s Youth about the Limitation of Power among the Three Branches of Government in Today’s Political World</h4>
          <p class="planner">
          (Presented by the ACE Committee)
          </p>
          <p class="copy">
          The constitutionally mandated separation of powers is essential to the balance of power in government. A government with a majority faction that wields disproportionate power over the nation shakes the structural integrity of our democracy.  Placing checks and balances among the legislative, executive and judicial branches of government was intended to prevent this threat. What happens, for instance, if the legislative and executive branches act as one? Are recent events an indication of impending structural breakdown of government as we know it, or simply a manifestation of the democratic ideal that the majority should rule?  This presentation will explore these and related questions within the context of law-related and civics education for high school and middle school students. Current events will be contemplated in light of our Constitution, government structure and relevant caselaw. Different approaches to teaching about the law and civics will be examined.
          </p>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Giovanni Campbell</strong> - Court of Common Pleas, Trial Division – Criminal, First Judicial District of Pennsylvania</li>
          <li><strong>Hon. Henry Lewandowski, III</strong> - Municipal Court, First Judicial District of Pennsylvania</li>
          <li><strong>Hon. David H. Oh</strong> - Councilman At-Large, Minority Whip, Philadelphia City Council</li>
          <li><strong>Prof. Louis S. Rulli</strong> - Practice Professor of Law and Clinical Director, University of Pennsylvania Law School</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
    <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 2</span></li>
          <li><a href="coursematerials/0445LU_DefensiveDrafting.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Defensive Document Drafting</h4>
          <p class="planner">
          (Presented by The Barristers’ Association of Philadelphia, Inc.)
          </p>
          <p class="copy">
          The creation of legal documents is integral to the practice of law; therefore, drafting skills are essential for every practicing lawyer. In this CLE program, panelists offer tips and techniques for drafting documents in all practice areas, including:
          </p>
          <ul>
            <li class="copy">Drafting for specific outcomes;</li>
            <li class="copy">Drafting for specific scenarios;</li>
            <li class="copy">Contracts and agreements;</li>
            <li class="copy">Drafting for challenges; and</li>
            <li class="copy">“Think like a Judge, be a better writer.”</li>
          </ul>
          <p class="copy">
          Words are used to advocate, inform, persuade and instruct. Drafting documents that accurately reflect what you mean requires planning, clarity, correct word choice, precise use of technical terms and as much simplicity as possible.
          </p>
         <h5 class="subheader">Course Planner/Panelist</h5>
         <ul class="square">
          <li><strong>Shabrei M. Parker</strong> - Of Counsel, Mincey &amp; Fitzpatrick, LLC</li>
         </ul>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>Hon. Lillian Harris Ransom</strong> - Senior Judge, Superior Court of Pennsylvania</li>
          <li><strong>Thomas O. Fitzpatrick</strong> - Partner, Mincey &amp; Fitzpatrick, LLC</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
    <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
          <li><span class="label warning radius">Studio 3</span></li>
          <li><a href="coursematerials/0445LV_AlternativeTreatmentsWC.pdf" target="_blank"><i class="icon-download" aria-hidden="true"></i> Course Materials</a></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Alternative Treatments in Chronic Pain Management: Treating Outside of Opioids</h4>
          <p class="planner">
          (Presented by the Workers' Compensation Section)
          </p>
          <p class="copy">
          In light of the opioid crisis and current statewide action to address this crisis, such as the proposed executive order of Governor Wolf, it has become very important for practitioners and judges alike to understand what alternatives are available to opioids.  Interventional pain medicine specialist, David Qu, M.D., will speak alongside defense attorney Andrea Rock and claimant attorney Alexis Handrich, concerning a wide variety of options available to treat chronic pain today.  
          </p>
         <h5 class="subheader">Panelists</h5>
         <ul class="square">
          <li><strong>David Qu, M.D., M.S.</strong> - Co-Founder, Pennsylvania Pain &amp; Spine Institute</li>
          <li><strong>Alexis C. Handrich</strong> - Pond Lehocky Stern Giordano</li>
          <li><strong>Andrea C. Rock</strong> - Marshall Dennehey Warner Coleman &amp; Goggin, P.C.</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
  
   </div> <!--item-->
   <div class="item saturday panel">
         <h4 class="schedule_title">Break</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">10:45 - 11 a.m.</span></li>
           <li><span class="label warning radius">Salon B</span></li>
         </ul>
   </div> <!--item-->
   <div class="item saturday sat11am12pm panel">
         <h4 class="schedule_title">Conference Conclusion and Plenary</h4>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
           <li><span class="label radius">11 a.m. - 12 p.m.</span></li>
           <li><span class="label warning radius">Salon D</span></li>
           <li><span class="label alert radius">Final Passport Giveaway</span></li>
         </ul>
             <div class="cleitem substantive">
     <div class="panel cle radius">
        <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-4">
          <li><strong>1 Substantive<br/>CLE | CJE Credit</strong></li>
        </ul>
       <hr>
           <h4 class="subheader schedule_subtitle">Old and New Ideas:  A Recipe for the Future Based Upon Experience of the Past</h4>
          <p class="copy">
          Tom Kline will discuss time-tested, universal ideas for the practice of law and the trial of cases that have guided him in his first 40 years of practice and provide his prescription for the future.
          </p>
         <h5 class="subheader">Featured Speaker</h5>
         <img src="images/ThomasKline.jpg" alt="Speaker Image" class="speaker-image">
         <ul class="square">
          <li><strong>Thomas R. Kline</strong> - Founding Partner, Kline &amp; Specter, PC</li>
         </ul>
     </div> 
     </div> <!-- cle item --> 
   </div> <!--item-->
 </div> <!-- ourHolder -->
 </div> <!-- row -->
</section>


<!-- Filter Module Triggered by Button on Sticky TopBar Nav For Schedule Page only -->

<div id="filterModul" class="reveal-modal"  data-reveal aria-labelledby="Schedule Filters" aria-hidden="true" role="dialog">
  <h3>Schedule Filters</h3>
<dl class="sub-nav" id="filterOptions">
  <dt><strong>Day Filter:</strong></dt>
  <dd class="active"><a href="#" class="all">Clear Filter</a></dd>
  <dd><a href="#" class="friday">Friday</a></dd>
  <dd><a href="#" class="saturday">Saturday</a></dd>
</dl>
<dl class="sub-nav timefilterOptions">
  <dt><strong>Time Filters:</strong></dt>
  <dd class="active"><a href="#" class="all">Clear Time Filter</a></dd>
</dl>
<hr/>
<dl class="sub-nav timefilterOptions">
  <dt><strong>Friday Time Filter:</strong></dt>
  <dd><a href="#" class="fri9am">9 a.m.</a></dd>
  <dd><a href="#" class="fri10am1130am">10 a.m.</a></dd>
  <dd><a href="#" class="fri1130am1pm">11:30 a.m.</a></dd>
  <dd><a href="#" class="fri1pm2pm">1 p.m.</a></dd>
  <dd><a href="#" class="fri215pm315pm">2:15 p.m.</a></dd>
  <dd><a href="#" class="fri330pm5pm">3:30 p.m.</a></dd>
  <dd><a href="#" class="fri5pm6pm">5 p.m.</a></dd>
  <dd><a href="#" class="fri6pm630pm">6 p.m.</a></dd>
  <dd><a href="#" class="fri71030pm">7 p.m.</a></dd>
</dl>
<hr/>
<dl class="sub-nav timefilterOptions">
  <dt><strong>Saturday Time Filter:</strong></dt>
  <dd><a href="#" class="sat8am">8 a.m.</a></dd>
  <dd><a href="#" class="sat8301030am">8:30 a.m.</a></dd>
  <dd><a href="#" class="sat945am1045am">9:45 a.m.</a></dd>
  <dd><a href="#" class="sat11am12pm">11 a.m.</a></dd>
</dl>
<!-- <hr/>
<dl class="sub-nav" id="filterOptions2">
  <dt><strong>CLE Filter:</strong></dt>
  <dd class="active"><a href="#" class="all">Clear Filter</a></dd>
  <dd><a href="#" class="ethics">Ethics</a></dd>
  <dd><a href="#" class="substantive">Substantive</a></dd>
</dl> -->
<a class="close-reveal-modal" aria-label="Close">&#215;</a>
</div> <!-- filterModul -->

<!-- End Filter Module Triggered by Button on Sticky TopBar Nav For Schedule Page only -->

<?php
include_once 'footer.php';
 ?>
